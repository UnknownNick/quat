/*
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package q;

import java.util.Random;

public class rational {

    public rational(int a, int b) {
        this.a = a;
        this.b = b;
    }
    
    
    public int a;
    public int b;
    //a/b
    
    @Override
    public String toString(){
        this.update();
        return( "" + a + ((b == 1)?(""):("/"+b)));
    }
    
    public void update(){
        int d;
        if(a!=0 && (d = GCD(Math.abs(a),Math.abs(b))) != 1 ){
            a /= d;
            b /= d;
        }
        if(b<0){
            a = -a;
            b = -b;
        }
    }
    
    public int GCD(int a, int b) {
        if (b==0) return a;
        return GCD(b,a%b);
    }
    
    public static rational mul(rational p, rational q) {
        return new rational(p.a * q.a, p.b * q.b);
    }
    
    public static rational rndInt(int bound){
        Random rng = new Random();
        rational r = new rational(rng.nextInt(bound+1)-(bound)/2,1);
        r.update();
        return r;
    }
}
