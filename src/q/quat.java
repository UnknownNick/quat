/*
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package q;

import java.util.Random;

public class quat {

    public quat(rational s, rational x, rational y, rational z) {
        this.s = s;
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public quat() {
      rational zero = new rational(0,0);
      this.s = zero;
      this.x = zero;
      this.y = zero;
      this.z = zero;  
    }
    
    public rational s, x, y, z;
    ////////////////////////////////////////////////////////////////////////////
    
    public void update(){
        s.update();
        x.update();
        y.update();
        z.update();
    }
    
    public boolean isOne(){
        this.update();
        return(s.a==1&&s.b==1&&x.a==0&&y.a==0&&z.a==0);
    }
    
    public boolean isZero(){
        return (s.a==0&&x.a==0&&y.a==0&&z.a==0);
    }
    
    @Override
    public String toString() {
        boolean prev = false;
        String st = "( ";
        if(s.a!=0) {
            st += s ;
            prev = true;
        }
        if(x.a!=0) {
            if(prev) st += x.a > 0 ? " + " : " ";
            st += x + "i";
            prev = true;
        }
        if(y.a!=0) {
            if(prev) st += y.a > 0 ? " + " : " ";
            st += y + "j";
            prev = true;
        }
        if(z.a!=0) {
            if(prev) st += z.a > 0 ? " + " : " ";
            st += z + "k";
        }
        st += " )";
        return st;
    }
    
    public static quat rndI(int bound){
        return new quat(rational.rndInt(bound),rational.rndInt(bound),rational.rndInt(bound),rational.rndInt(bound));
    }
    
    public static quat rndI(int bound, float oneChance){
        Random rng = new Random();
        if(oneChance != 0 && rng.nextInt((int)(100f/oneChance))<100)
            return one();
        else 
            return rndI(bound);
    }
    
    public static quat zero(){
        return new quat(new rational(0,1),new rational(0,1),new rational(0,1),new rational(0,1));
    }
    
    public static quat one(){
        return new quat(new rational(1,1),new rational(0,1),new rational(0,1),new rational(0,1));
    }
    
    public static rational normsq(quat a){
        return sum(mul(a.s,a.s),mul(a.x,a.x),mul(a.y,a.y),mul(a.z,a.z));
    }
    
    public static quat conjugate(quat a){
        return new quat(a.s,neg(a.x),neg(a.y),neg(a.z));
    }
    
    public static quat mul(quat a, quat b) {
        return new quat(
                sum(mul(a.s,b.s),neg(mul(a.x,b.x)),neg(mul(a.y,b.y)),neg(mul(a.z,b.z))) , //s
                sum(mul(a.s,b.x),mul(b.s,a.x),sum(mul(a.y,b.z),neg(mul(b.y,a.z)))), //x
                sum(mul(a.s,b.y),mul(b.s,a.y),sum(mul(a.z,b.x),neg(mul(b.z,a.x)))), //y
                sum(mul(a.s,b.z),mul(b.s,a.z),sum(mul(a.x,b.y),neg(mul(b.x,a.y))))  //z
        );
    }
    
    public static quat sum(quat a, quat b){
        return new quat(sum(a.s,b.s),sum(a.x,b.x),sum(a.y,b.y),sum(a.z,b.z));
    }
    
    public static rational mul(rational a, rational b){
        return rational.mul(a, b);
    }
    
    public static rational sum(rational a, rational b, rational c, rational d){
        return sum(a,sum(b,c,d));
    }
    
    public static rational sum(rational a, rational b, rational c){
        return sum(a,sum(b,c));
    }
    
    public static rational sum(rational a, rational b){
        rational temp = new rational(a.a*b.b+b.a*a.b,a.b*b.b);
        temp.update();
        return temp;
    }
    
    public static rational neg(rational a) {
        return new rational(-a.a,a.b);
    }
}
