/*
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package q;

import java.util.Random;

public class equations {
    public String[] labels = {"x","y","z","w","p","q","r","s","t","u","v"};
    public quat[] unknowns;
    public term[][] terms; //[equation][term]
    public quat[] vals;
    public int eq, tr;
    public int bound;
    public float zeroChance;
    public float oneChance; 
    
    public void print(){
        System.out.println("========================");
        for(int e = 0; e < terms.length ;e++){
            String equation = "[" + (e+1) + "] ";
            boolean prev = false;
            for(int t = 0 ; t < terms[e].length ; t++){
                if(!terms[e][t].eval().isZero()){
                    if(prev) equation += " + ";
                    equation += terms[e][t];
                    prev = true;
                }
            }
            equation += " = " + vals[e];
            System.out.println(equation);
        }
        System.out.println("------------------------");
        for(int i = 0; i < unknowns.length ;i++) {
            System.out.println("["+label(i)+":" + unknowns[i] +"]");
        }
        System.out.println("========================");
    }
    
    public void setup(int width, int height, int bound, float zeroChance, float oneChance){
        tr = width - 1;
        eq = height;
        this.bound = bound;
        this.zeroChance = zeroChance;
        this.oneChance = oneChance;
        this.makeUnknowns();
        this.makeTerms(this.zeroChance,this.oneChance);
        this.eval();
    }
    
    public void eval(){
        vals = new quat[eq];
        for(int i = 0; i < vals.length ;i++) {
            quat temp = quat.zero();
            for(term q : terms[i]) {
                temp = quat.sum(temp, q.eval());
            }
            vals[i] = temp;
        }
    }
    
    public void makeTerms(float zeroChance,float oneChance){
        Random rng = new Random();
        terms = new term[eq][tr];
        for(int e = 0; e < terms.length; e++){
            for(int t = 0; t < terms[e].length; t++){
                term temp;
                if(zeroChance != 0f && rng.nextInt((int)(100f/zeroChance))<100){
                    temp = term.zero(label(t));
                } else {
                    temp = new term(quat.rndI(bound,oneChance),unknowns[t],quat.rndI(bound,oneChance),label(t));
                }
                terms[e][t] = temp;
            }
        }
    }
    
    public void makeUnknowns(){
        unknowns = new quat[tr];
        for(int i = 0; i < unknowns.length ; i++){
            unknowns[i] = quat.rndI(bound);
        }
    }
    
    public String label(int index){
        if(index < labels.length)
            return labels[index];
        else 
            return (labels[index%labels.length] + "" + index/labels.length);
    }
}
