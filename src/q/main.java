/*
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package q;

public class main {

    static char[] valid = {'w','h','b','0','1','e'};
    static Object[] settings = {3,2,10,1f/6,1f/2};
    
    public static void main(String[] args) {
        if(args.length > 0) {
            if(args[0].contains("help")) {
                System.out.print("use: java -jar <jarname> [OPTIONS..]\n"
                        + "options:\n"
                        + " w: assemblage width <=> amount of unknowns\n"
                        + "     default 3\n"
                        + " h: assemblage height <=> amount of equations\n"
                        + "     default 2\n"
                        + " b: bound for coeficients where integer k\\in<-bound/2;bound/2>\n"
                        + "     default 10\n"
                        + " 0: float chance for coeficient 0\n"
                        + "     default 1/6\n"
                        + " 1: float chance for coeficient 1\n"
                        + "     default 1/2\n"
                        + " e: assemblage size, overrides w and h\n"
                        + "     default 2 (equations)\n"
                        + " first argument contains \"help\": display this message\n"
                        + "\n"
                        + " example: java -jar <jarname> wh0b 5 4 0.05 100\n");
                System.exit(0);
            }
            if(args.length == 1) {
                System.err.println("Unrecognized singular argument, try --help.");
                System.exit(1);
            }
        }
        if(args.length > 1){
            char[] ar = args[0].toCharArray();
            int[] indexes = new int[settings.length];
            if(args.length == ar.length + 1) {
                int found = 0;
                for(char v : valid){
                    boolean present = false;
                    for(char a : ar) {
                        if(a==v) {
                            if(present) {
                                System.err.println("Duplicate argument : " + a + ", exiting!");
                                System.exit(1);
                            } else {
                                present = true;
                                found++;
                            }
                        }
                    }
                }
                if(found < ar.length) {
                    System.err.println("Unrecognized arguments, exiting!");
                    System.exit(1);
                }
                int e = -1;
                for(int i = 0; i < ar.length ;i++) {
                    switch(ar[i]){
                        case 'w':
                            try {
                                settings[0] = Integer.parseInt(args[1+i]);
                            } catch(NumberFormatException ex) {
                                System.err.println("Error parsing integer: " + args[1+i] + ", exiting!");
                                System.exit(1);
                            }
                            break;
                        case 'h':
                            try {
                                settings[1] = Integer.parseInt(args[1+i]);
                            } catch(NumberFormatException ex) {
                                System.err.println("Error parsing integer: " + args[1+i] + ", exiting!");
                                System.exit(1);
                            }
                            break;
                        case 'b':
                            try {
                                settings[2] = Integer.parseInt(args[1+i]);
                            } catch(NumberFormatException ex) {
                                System.err.println("Error parsing integer: " + args[1+i] + ", exiting!");
                                System.exit(1);
                            }
                            break;
                        case '0':
                            try {
                                settings[3] = Float.parseFloat(args[1+i]);
                            } catch(NumberFormatException ex) {
                                System.err.println("Error parsing float: " + args[1+i] + ", exiting!");
                                System.exit(1);
                            }
                            break;
                        case '1':
                            try {
                                settings[4] = Float.parseFloat(args[1+i]);
                            } catch(NumberFormatException ex) {
                                System.err.println("Error parsing float: " + args[1+i] + ", exiting!");
                                System.exit(1);
                            }
                            break;
                        case 'e':
                            e = i;
                    }
                }
                if(e > -1){
                    try {
                        settings[0] = Integer.parseInt(args[1+e]) + 1;
                    } catch(NumberFormatException ex) {
                        System.err.println("Error parsing integer: " + args[1+e] + ", exiting!");
                        System.exit(1);
                    }
                    try {
                        settings[1] = Integer.parseInt(args[1+e]);
                    } catch(NumberFormatException ex) {
                        System.err.println("Error parsing integer: " + args[1+e] + ", exiting!");
                        System.exit(1);
                    }
                }
            } else {
                System.err.println("Mismatched amount of arguments, exiting!");
                System.exit(1);
            }
        }
        if((int)settings[0] < 1) {
            System.err.println("w must be greater than 1 and/or e positive");
            System.exit(1);
        }
        if((int)settings[1] < 1) {
            System.err.println("h and e must be positive");
            System.exit(1);
        }
        if((int)settings[2] < 1) {
            System.err.println("bound must be positive");
            System.exit(1);
        }
        if((float)settings[3] < 0 || (float)settings[3] > 1 ||
           (float)settings[4] < 0 || (float)settings[4] > 1) {
            System.err.println("chnances must be \\in <0;1>");
            System.exit(1);
        }
        
        equations eq = new equations();
        eq.setup((int)settings[0], (int)settings[1], (int)settings[2], (float)settings[3],(float)settings[4]);
        eq.print();
    }
    
}
