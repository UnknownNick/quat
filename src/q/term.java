/*
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package q;

public class term {
    public quat left, center, right;
    String label;
    
    public term(quat left, quat center, quat right, String label){
        this.left = left;
        this.center = center;
        this.right = right;
        this.label = label;
    }
    
    @Override
    public String toString(){
        String s = "";
        if(!left.isZero() && !right.isZero()){
            if(!left.isOne()){
                s += left;
            }
            s += label;
            if(!right.isOne()){
                s += right;
            }
        }
        return s;
    }
    
    public static term zero(String label){
        term t = new term(quat.zero(),quat.zero(),quat.zero(),label);
        return t;
    }
    
    public quat eval() {
        return quat.mul(left, quat.mul(center,right));
    }
}
